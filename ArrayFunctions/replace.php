<?php
$base = array("orange", "banana", "apple", "raspberry");
$replacements = array(0 => "pineapple", 3 => "cherry");
$replacements2 = array(0 => "grape","ABC",4=>"Mango","Apple");

$basket = array_replace($base, $replacements, $replacements2);
echo "<pre>";
print_r($basket);
echo "</pre>";
echo "<hr>";
$basket = array_replace_recursive($base, $replacements, $replacements2);
echo "<pre>";
print_r($basket);
echo "</pre>";
?>