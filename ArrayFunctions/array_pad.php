<?php
$input = array(12, 10, 9);

$result = array_pad($input, 5, 0);
// result is array(12, 10, 9, 0, 0)
echo '<pre>';
print_r ($result);
echo '<pre>';

$result = array_pad($input, -9, -1);
// result is array(-1, -1, -1, -1, 12, 10, 9)
echo '<pre>';
print_r ($result);
echo '<pre>';

$result = array_pad($input, 5, "noop");
echo '<pre>';
print_r ($result);
echo '<pre>';
// not padded
?>